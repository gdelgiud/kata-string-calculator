# Kata: String Calculator

*Before you start*

+ Try not to read ahead.
+ Do one task at a time. The trick is to learn to work incrementally.
+ There is no need to test for invalid inputs for this kata, assume the string you receive is correctly formatted.

## Iterations

### Iteration 1

Create a simple String calculator with a method `int Add(string numbers)`
+ The method can take 0, 1 or 2 comma-separated numbers, and will return their sum (for an empty string it will return 0) for example "" or "1" or "1,2"
+ Start with the simplest test case of an empty string and move to one and two numbers
+ Remember to solve things as simply as possible
+ Remember to refactor after each passing test

### Iteration 2

Allow the Add method to handle an unknown amount of numbers.


### Iteration 3

Allow the Add method to handle newlines between numbers instead of commas.
+ The following input is valid: "1\n2,3" (will equal 6)
+ The following input is **not** valid: "1,\n" (no need to handle this in your code)

### Iteration 4

Support different delimiters
+ To change a delimiter, the beginning of the string will contain a separate line that looks like this: “//[delimiter]\n[numbers...]” for example “//;\n1;2” should return three since the delimiter is ‘;’.
+ The first line is optional, so all existing scenarios should still be supported, (existing tests should still pass).

### Iteration 5

Calling `Add` with a negative number should throw an exception "negatives not allowed". The exception message should include the negative that was passed. If there are multiple negatives, list all of them in the message.

## Retrospective

+ Did you ever write more code than you needed to make the current tests pass?
+ Did you ever have more than one failing test at a time?
+ Did the tests fail unexpectedly at any point? If so, why?
+ How much did writing the tests slow you down?
+ Did you write more tests than you would have if you had coded first and written tests afterwards?
+ Are you happy with the design of the code you ended up with? Should you have refactored it more often?